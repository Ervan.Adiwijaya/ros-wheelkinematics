# Simple Documentation For ROS Project

## Project Environment
* Ubuntu 18.04
* ROS Melodic
* catkin
* Text Editor

## Project Setup
1. Add `source /opt/ros/melodic/setup.bash` on your `.bashrc` and source to `.bashrc`
2. Create a workspace (~/[workspace]) and clone this repo to your workspace
3. Move to your workspace dir and run `catkin_make`
